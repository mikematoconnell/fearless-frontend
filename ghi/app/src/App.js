import React from "react"
import Nav from "./Nav"
import AttendeesList from "./AttendeesList"
import LocationForm from "./LocationForm"
import ConferenceForm from "./ConferenceForm"
import AttendeesSingupForm from "./AttendeesSignupForm"
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom"
import MainPage from "./MainPage"
import PresentationForm from "./PresentationForm"

function App(props) {
  let {attendees} = props
  if (attendees === undefined) {
    return null;
  } 
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendeesSingupForm />} />
            <Route path="" element={<AttendeesList attendees={attendees} />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="presentation">
            <Route path="" element={<PresentationForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;