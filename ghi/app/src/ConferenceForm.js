import React from "react"

class ConferenceForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: "",
            starts: "",
            ends: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            locations: [],
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this)
        this.handleEndChange = this.handleEndChange.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this)
        this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name:value})
    }
    handleStartChange(event) {
        const value = event.target.value
        this.setState({starts:value})
    }
    handleEndChange(event) {
        const value = event.target.value
        this.setState({ends:value})
    }
    handleDescriptionChange(event) {
        const value = event.target.value
        this.setState({description:value})
    }
    handleMaxAttendeesChange(event) {
        const value = event.target.value
        this.setState({maxAttendees:value})
    }
    handleMaxPresentationChange(event) {
        const value = event.target.value
        this.setState({maxPresentations:value})
    }
    handleLocationChange(event) {
        const value = event.target.value
        this.setState({location:value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        // triple dot means copy
        const data = {...this.state};
        data.max_presentations = data.maxPresentations
        data.max_attendees = data.maxAttendees
        delete data.maxAttendees
        delete data.maxPresentations


        // fetched and saved all of the states into the state of the form instance, on submit this is useless so delete
        delete data.locations;
        console.log(data);        
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {
                name: "",
                starts: "",
                ends: "",
                description: "",
                maxPresentations: "",
                maxAttendees: "",
            }
            this.setState(cleared)
        }
    }

    async componentDidMount () {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations})
            
        }
      }
    
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={this.state.name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb3">
                        <input onChange={this.handleStartChange} placeholder="Start" required type="date" id="start" name="starts" className="form-control" value={this.state.starts}/>
                        <label htmlFor="start">Start Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleEndChange} placeholder="End" required type="date" id="end" name="ends" className="form-control" value={this.state.ends}/>
                        <label htmlFor="end">End Date</label>
                    </div>
                    <div>
                        <label>Description</label>
                        <textarea onChange={this.handleDescriptionChange} required name="description" id="description" className="form-control" value={this.state.description}/>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxPresentationChange} placeholder="Max Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" value={this.state.maxPresentations}/>
                        <label htmlFor="max_presentations">Max Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxAttendeesChange} placeholder="Max Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" value={this.state.maxAttendees}/>
                        <label htmlFor="max_attendees">Max Attendees</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={this.handleLocationChange} require id="location" name="location" className="form-select" value={this.state.location}>
                            <option value="">Choose a location</option>
                            {this.state.locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                )                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
      );
    }
  }
export default ConferenceForm