import React from "react"

class PresentationForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            presenter_name: "",
            company_name: "",
            presenter_email: "",
            title: "",
            synopsis: "",
            conferences: [],
        }
        this.handlePresenterName = this.handlePresenterName.bind(this);
        this.handleCompanyName = this.handleCompanyName.bind(this)
        this.handlePresenterEmail = this.handlePresenterEmail.bind(this)
        this.handleTitle = this.handleTitle.bind(this)
        this.handleSynopsis = this.handleSynopsis.bind(this)
        this.handleConference = this.handleConference.bind(this)
        this.handleSubmit= this.handleSubmit.bind(this)
        
    }
    handlePresenterName(event) {
        const value = event.target.value;
        this.setState({presenter_name:value})
    }
    handleCompanyName(event) {
        const value = event.target.value;
        this.setState({company_name:value})
    }
    handlePresenterEmail(event) {
        const value = event.target.value
        this.setState({presenter_email:value})
    }
    handleTitle(event) {
        const value = event.target.value
        this.setState({title:value})
    }
    handleSynopsis(event) {
        const value = event.target.value
        this.setState({synopsis:value})
    }
    handleConference(event) {
        const value = event.target.value
        this.setState({conference:value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        // triple dot means copy
        const data = {...this.state};
        console.log(data)
        
        delete data.conferences;
        console.log(data); 

        const conId = data.conference

        const presentationURL = `http://localhost:8000/api/conferences/${conId}/presentations/`;
            const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                }
            };
        const response = await fetch(presentationURL, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            }
            this.setState(cleared)
        
        }else{
            console.error("Error Not a good Response")
        }
    }

    async componentDidMount () {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences})
            
        }
      }
    
      render () {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterName} placeholder="Presenter name" required type="tet" name="presenter_name" id="presenter_name" className="form-control" value={this.state.presenter_name}/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"value={this.state.presenter_email}/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCompanyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"value={this.state.company_name}/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={this.state.title}/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={this.handleSynopsis} className="form-control" id="synopsis" rows="3" name="synopsis" value={this.state.synopsis}/>
              </div>
              <div className="mb-3">
                <select value={this.state.conference} onChange={this.handleConference}required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {this.state.conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        )
    }
}


export default PresentationForm